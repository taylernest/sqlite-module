package com.taylernest.sqlite;

import org.refactoral.unibot.AppProperties;
import sx.blah.discord.Discord4J;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.modules.IModule;
import sx.blah.discord.util.LogMarkers;

import java.io.*;
import java.util.Properties;

public class PluginCore implements IModule {

    public boolean enable(IDiscordClient client) {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            Discord4J.LOGGER.error(LogMarkers.MODULES, "Can't load JDBC driver: class not found!");
            return false;
        }
        try {
            Class.forName("com.taylernest.unibot.AppProperties");
            Properties properties = AppProperties.getProperties();
        } catch (ClassNotFoundException e) {
            File propFile = new File("app.properties");
            Properties properties = new Properties();
            try {
                FileInputStream fis = new FileInputStream(propFile);
            } catch (FileNotFoundException e1) {
                Discord4J.LOGGER.warn(LogMarkers.MODULES, "Properties file not found or unaccessible! Trying to create new one");
                try {
                    boolean isCreated = propFile.createNewFile();
                    if(!isCreated) {
                        throw new IOException("File already exists");
                    }
                    FileOutputStream fos = new FileOutputStream(propFile);
                    properties.setProperty("sqlite.connection", "database.db");
                } catch (IOException e2) {
                    Discord4J.LOGGER.error(LogMarkers.MODULES, "Can't create new properties file!");
                    return false;
                }
            }
        }
        return true;
    }

    public void disable() {

    }

    public String getName() {
        return "SQLite";
    }

    public String getAuthor() {
        return "TaylerNest";
    }

    public String getVersion() {
        return "1.0";
    }

    public String getMinimumDiscord4JVersion() {
        return "2.6.1";
    }
}
