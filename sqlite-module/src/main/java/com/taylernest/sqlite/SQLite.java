package com.taylernest.sqlite;

import com.refactoral.unibot.internal.api.IUser;
import com.refactoral.unibot.api.Log;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
 * Provider.sqlite.execute("insert into users values (" + e.getUser().getName() + ", " + e.getUser().getID() + ")");
 * Provider.sqlite.execute("delete from users where id=" + e.getUser().getID());
 * commands.add(new IConsoleCommand() {

            @Override
            public String getTrigger() {
                return "sql";
            }

            @Override
            public void execute(String fullCommand) {
                try {
                    String query = fullCommand.substring(3);
                    try {
                        Provider.sqlite.executeUpdate(query);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    System.out.println("Invalid statement");
                }
            }

        });
 */

@SuppressWarnings("SqlDialectInspection")
public class SQLite {
    public static SQLModel INSTANCE;
    private static Connection database = null;

    private SQLModel() {
    }

    public static void load(String db) throws SQLException {
        database = DriverManager.getConnection(db);
        INSTANCE = new SQLModel();
    }

    public Statement getStatement() throws SQLException {
        return database.createStatement();
    }

    public void close() {
        try {
            Provider.sqlite.close();
        } catch (SQLException ignored) {
        } finally {
            try {
                database.close();
            } catch (SQLException ignored) {
            }
        }
    }

    public ResultSet getUsers() {
        try {
            return Provider.sqlite.executeQuery("select * from users");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public void updateUsers() throws SQLException {
        ResultSet users = Provider.sqlite.executeQuery("select * from users");
        List<IUser> userscl = Provider.client.getUsers();
        List<String> ids = new ArrayList<>();
        List<String> usrs = new ArrayList<>();
        while (!users.isAfterLast()) {
            ids.add(users.getString(2));
            usrs.add(users.getString(1));
            users.next();
        }
        for (IUser user : userscl) {
            if (ids.contains(user.getID())) {
                if (!usrs.contains(user.getName())) {
                    Log.info("New username for user ID " + user.getID() + ": " + user.getName());
                    Provider.sqlite.executeUpdate("update users set name='" + user.getName() + "' where id='" + user.getID() + "'");
                }
            } else {
                Provider.sqlite.executeUpdate("insert into users (name, id) values ('" + user.getName() + "', '" + user.getID() + "')");
                Log.info("Added user " + user.getName() + " to database. ID: " + user.getID());
            }
        }
        users.close();
    }
}
